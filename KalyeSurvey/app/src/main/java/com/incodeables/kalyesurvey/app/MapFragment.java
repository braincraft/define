package com.incodeables.kalyesurvey.app;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.incodeables.kalyesurvey.MainActivity;
import com.incodeables.kalyesurvey.R;

import java.util.Random;


public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    public static View view;
    public GoogleMap mMap;
    private LocationManager locationManager;
    private static final long MIN_TIME = 200;
    private static final float MIN_DISTANCE = 100;
    private boolean isGoing;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        isGoing = false;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
            ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        } catch (Exception e) {
        }
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment

        ((ImageView)view.findViewById(R.id.force_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isGoing) {
                    earn(3);
                }
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER


    }

    @Override
    public void onLocationChanged(Location location) {

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            mMap.animateCamera(cameraUpdate);
            if(location!=null){
                if(location.hasSpeed()){
                    //Toast.makeText(getContext(),String.valueOf(location.getSpeed()),Toast.LENGTH_SHORT).show();
                    if(!isGoing)
                    {
                        //Ads 10 m/s
                        if(location.getSpeed()<=3)
                        {
                            earn(1);
                        }
                        else if(location.getSpeed()<=8.5){
                            earn(2);
                        }
                    }
                }
            }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void earn(int itemType){
        // Call this to initialize survey or add
        // 1 for Ads
        // 2 for Surveys
        // 3 for Random (force)
        isGoing=true; //Use this for sharing, avoiding multiple items to run at once
        MainActivity activity = (MainActivity)getActivity();
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();

        if(itemType==1){
            //Fragment selectedFragment = new AdViewFragment();
            //fragmentTransaction.replace(R.id.content_frame, selectedFragment);
            //fragmentTransaction.commit();
        }
        else if(itemType==2){
            Toast.makeText(getContext(),"Surveys",Toast.LENGTH_SHORT).show();
            Fragment selectedFragment = new SurveyFragment();
            activity.setFragment((SurveyFragment) selectedFragment);
            fragmentTransaction.replace(R.id.content_frame, selectedFragment);
            fragmentTransaction.commit();
        }

        else if(itemType==3){
            //Toast.makeText(getContext(),"Forced",Toast.LENGTH_SHORT).show();
            //Random:
            /*
            Random rand = new Random();
            int x = rand.nextInt(2)+1;

            if(x%2==0)
            {
                //create ads
            }
            else
            {*/
                //create surveys
                Toast.makeText(getContext(),"Surveys",Toast.LENGTH_SHORT).show();
                Fragment selectedFragment = new SurveyFragment();
                activity.setFragment((SurveyFragment) selectedFragment);
                fragmentTransaction.replace(R.id.content_frame, selectedFragment);
                fragmentTransaction.commit();

            //}

        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
