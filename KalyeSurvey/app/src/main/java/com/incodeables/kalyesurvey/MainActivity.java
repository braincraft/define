package com.incodeables.kalyesurvey;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.incodeables.kalyesurvey.app.MapFragment;
import com.incodeables.kalyesurvey.app.NavigationDrawerFragment;
import com.incodeables.kalyesurvey.app.SplashScreen;
import com.incodeables.kalyesurvey.app.SurveyFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MapFragment.OnFragmentInteractionListener {

    private static final String TAG = "VOICE";

    private SurveyFragment fragment;
    private Handler handler;
    private SpeechRecognizer sr;
    private Intent intent;

    private Toolbar toolbar;

    private ActionBarDrawerToggle drawerToggle;
    public DrawerLayout drawerLayout;
    private NavigationDrawerFragment drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, SplashScreen.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        setUpToolbar();
        setUpDrawer();
        setUpSpeechRecognizer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sr.stopListening();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            sr.destroy();
        }
        catch (Exception e)
        {
            Log.e(TAG,"Exception:"+e.toString());
        }
    }

    public void setFragment(SurveyFragment fragment) {
        this.fragment = fragment;
    }

    public void setUpSpeechRecognizer() {
        handler = new Handler(Looper.getMainLooper());

        sr = SpeechRecognizer.createSpeechRecognizer(this);
        sr.setRecognitionListener(new Listener());
    }

    public void listenToAnswer() {
        intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "com.incodeables.kalyesurvey");
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);

        handler.post(new Runnable() {
            @Override
            public void run() {
                sr.startListening(intent);
            }
        });
    }

    private void setUpToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.app_name));
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
    }

    private void setUpDrawer() {
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.nav_drwr_fragment);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };

        drawerFragment.setUpDrawer(this, R.id.nav_drwr_fragment, drawerLayout, drawerToggle, toolbar);
    }

    class Listener implements RecognitionListener {
        public void onReadyForSpeech(Bundle params)
        {
            Log.d(TAG, "onReadyForSpeech");
        }
        public void onBeginningOfSpeech()
        {
            Log.d(TAG, "onBeginningOfSpeech");
        }
        public void onRmsChanged(float rmsdB)
        {
            Log.d(TAG, "onRmsChanged");
        }
        public void onBufferReceived(byte[] buffer)
        {
            Log.d(TAG, "onBufferReceived");
        }
        public void onEndOfSpeech()
        {
            Log.d(TAG, "onEndofSpeech");
            Toast.makeText(getApplicationContext(), "Processing response...", Toast.LENGTH_LONG).show();
        }

        public void onError(int error)
        {
            Log.d(TAG,  "error " +  error);
        }
        public void onResults(Bundle results)
        {
            Log.d(TAG, "onResults " + results);
            ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            if (!data.isEmpty()) {
                if (data.contains(getString(R.string.yes).toLowerCase())) {
                    //Toast.makeText(getApplicationContext(), "She said yes!", Toast.LENGTH_SHORT).show();
                    fragment.setYes(0);
                }
                else if (data.contains(getString(R.string.no).toLowerCase())) {
                    //Toast.makeText(getApplicationContext(), "Oh dang it!", Toast.LENGTH_SHORT).show();
                    fragment.setNo(0);
                }
            }

            fragment.removeDone();
            fragment.readNext();
        }
        public void onPartialResults(Bundle partialResults)
        {
            Log.d(TAG, "onPartialResults");
        }

        public void onEvent(int eventType, Bundle params)
        {
            Log.d(TAG, "onEvent " + eventType);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
