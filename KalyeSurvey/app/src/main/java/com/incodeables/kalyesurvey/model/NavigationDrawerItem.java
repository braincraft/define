package com.incodeables.kalyesurvey.model;

import com.incodeables.kalyesurvey.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianeyee on 24/02/2016.
 */
public class NavigationDrawerItem {
    private String title;
    private int imageId;
    private int position;
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public int getPosition(){
        return position;
    }

    public void setPosition(int x){
        this.position=x;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public static List<NavigationDrawerItem> getData() {
        List<NavigationDrawerItem> dataList = new ArrayList<>();

        int[] imageIds = getImages();
        String[] titles = getTitles();

        for (int i=0; i<titles.length; i++) {
            NavigationDrawerItem navItem = new NavigationDrawerItem();
            navItem.setTitle(titles[i]);
            navItem.setImageId(imageIds[i]);
            navItem.setPosition(i);
            dataList.add(navItem);
        }
        return dataList;
    }

    private static int[] getImages() {
        return new int[]{R.drawable.earn,
                        R.drawable.rewards,
                        R.drawable.ads_viewed,
                        R.drawable.surveys,
                        R.drawable.about,
                        R.drawable.exit
        };
    }

    private static String[] getTitles() {
        return new String[]{"Earn Now",
                            "Rewards",
                            "Ads Viewed",
                            "Surveys",
                            "About",
                            "Exit"
        };
    }
}
