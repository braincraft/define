package com.incodeables.kalyesurvey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.incodeables.kalyesurvey.R;
import com.incodeables.kalyesurvey.model.Question;

import java.util.List;

/**
 * Created by christianeyee on 24/02/2016.
 */
public class QuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int BI_QUESTION = 0;
    private static final int MULTI_QUESTION = 1;

    private Context context;
    private List<Question> questions;
    private LayoutInflater inflater;

    public QuestionAdapter(Context context, List<Question> questions) {
        this.context = context;
        this.questions = questions;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;

        if (viewType == BI_QUESTION) {
            holder = new BiViewHolder(inflater.inflate(R.layout.bi_question_item, parent, false));
        } else {
            holder = new MultiViewHolder(inflater.inflate(R.layout.multi_question_item, parent, false));
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Question current = questions.get(position);

        if (getItemViewType(position) == BI_QUESTION) {
            ((BiViewHolder)holder).setData(current, position);
        }
        else {
            ((MultiViewHolder)holder).setData(current, position);
        }

    }

    @Override
    public int getItemViewType (int position) {
        return questions.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public String getItemText(int position) {
        return questions.get(position).getText();
    }

    public void removeItem(int position) {
        questions.remove(position);
        notifyItemRemoved(position);
        // without this, inconsistent index
        notifyItemRangeChanged(position, questions.size());

        // no animation since all views are redrawn
        // notifyDataSetChanged();
    }

    public void addItem(int position, Question question) {
        questions.add(position, question);
        notifyItemInserted(position);
        // without this, inconsistent index
        notifyItemRangeChanged(position, questions.size());

        // no animation since all views are redrawn
        // notifyDataSetChanged();
    }

    public void setYes(int position) {
        questions.get(position).setYes(true);
    }

    public void setNo(int position) {
        questions.get(position).setYes(false);
    }

    class BiViewHolder extends RecyclerView.ViewHolder {
        Question current;
        int position;

        private TextView text;
        private RadioButton yes;
        private RadioButton no;

        public BiViewHolder(View itemView) {
            super(itemView);
            this.text = (TextView)itemView.findViewById(R.id.textView);
            this.yes = (RadioButton)itemView.findViewById(R.id.answer_yes);
            this.no = (RadioButton)itemView.findViewById(R.id.answer_no);
        }
        public void setData(Question current, int position) {
            this.position = position;
            this.current = current;
            this.text.setText(current.getText());
            if (current.isYes() != null) {
                this.yes.setChecked(current.isYes());
                this.no.setChecked(!current.isYes());
            }
        }
    }

    class MultiViewHolder extends RecyclerView.ViewHolder {
        Question current;
        int position;

        private TextView text;
        private Button agree;
        private Button agree_strong;
        private Button disagree;
        private Button disagree_strong;

        public MultiViewHolder(View itemView) {
            super(itemView);
            this.text = (TextView)itemView.findViewById(R.id.textView);
        }

        public void setData(Question current, int position) {
            this.position = position;
            this.current = current;
            this.text.setText(current.getText());
        }
    }
}
