package com.incodeables.kalyesurvey.DB;

/**
 * Created by GianCarlo on 2/28/2016.
 */
public class Item {
    private int id,type;
    private String date,sponsor,coupons;
    private float points;

    public Item(int id, String date, int type, String sponsor, float points, String coupons ){
        this.id=id;
        this.type=type;
        this.date=date;
        this.sponsor=sponsor;
        this.points=points;
        this.coupons=coupons;
    }

    public int getId(){
        return this.id;
    }

    public String getDate(){
        return this.date;
    }

    public int getType(){
        return this.type;
    }

    public String getSponsor(){
        return this.sponsor;
    }

    public float getPoints(){
        return this.points;
    }

    public String getCoupons(){
        return this.coupons;
    }
}
