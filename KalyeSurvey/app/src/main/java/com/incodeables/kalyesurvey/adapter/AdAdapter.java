package com.incodeables.kalyesurvey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.incodeables.kalyesurvey.R;
import com.incodeables.kalyesurvey.model.Ad;

import java.util.Collections;
import java.util.List;

/**
 * Created by christianeyee on 28/02/2016.
 */
public class AdAdapter extends RecyclerView.Adapter<AdAdapter.MyViewHolder> {

    private List<Ad> mDataList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public AdAdapter(Context context, List<Ad> data) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.mDataList = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.reward_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Ad current = mDataList.get(position);

        holder.title.setText(current.getTitle());
        holder.date.setText(current.getDate());
        holder.sponsor.setText(current.getSponsor());
        holder.reward.setText(current.getReward());
    }


    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, date, sponsor, reward;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            sponsor = (TextView) itemView.findViewById(R.id.sponsor);
            reward = (TextView) itemView.findViewById(R.id.reward);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
