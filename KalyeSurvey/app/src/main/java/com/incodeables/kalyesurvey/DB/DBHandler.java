package com.incodeables.kalyesurvey.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by GianCarlo on 2/28/2016.
 */
public class DBHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "itemManager",
            TABLE_ITEMS="items",
            KEY_ID="id",
            ITEM_DATE="idate",
            ITEM_TYPE = "item_type",
            SPONSOR="sponsor",
            POINTS = "points",
            COUPONS="coupons";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_ITEMS + "(" + "id int, idate text, item_type, sponsor text, points real, coupon text " + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_ITEMS);
        onCreate(db);
    }

    public void createItem(Item item){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, item.getId());
        values.put(ITEM_DATE, item.getDate());
        values.put(ITEM_TYPE, item.getType());
        values.put(SPONSOR, item.getSponsor());
        values.put(POINTS, item.getPoints());
        values.put(COUPONS,item.getCoupons());
        db.insert(TABLE_ITEMS,null,values);
        db.close();
    }


}
