package com.incodeables.kalyesurvey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.incodeables.kalyesurvey.R;
import com.incodeables.kalyesurvey.model.Earning;

import java.util.Collections;
import java.util.List;

/**
 * Created by christianeyee on 28/02/2016.
 */
public class EarningAdapter extends RecyclerView.Adapter<EarningAdapter.MyViewHolder> {

    private List<Earning> mDataList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public EarningAdapter(Context context, List<Earning> data) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.mDataList = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.earning_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Earning current = mDataList.get(position);

        holder.date.setText(current.getDate());
        holder.sponsor.setText(current.getSponsor());
        holder.reward.setText(current.getReward());
    }


    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView date, sponsor, reward;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            sponsor = (TextView) itemView.findViewById(R.id.sponsor);
            reward = (TextView) itemView.findViewById(R.id.reward);
        }
    }
}
