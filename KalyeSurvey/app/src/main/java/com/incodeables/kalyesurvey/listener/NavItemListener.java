package com.incodeables.kalyesurvey.listener;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.incodeables.kalyesurvey.MainActivity;
import com.incodeables.kalyesurvey.R;
import com.incodeables.kalyesurvey.adapter.NavigationDrawerAdapter;
import com.incodeables.kalyesurvey.app.AboutFragment;
import com.incodeables.kalyesurvey.app.AdsFragment;
import com.incodeables.kalyesurvey.app.EarningsFragment;
import com.incodeables.kalyesurvey.app.MapFragment;
import com.incodeables.kalyesurvey.model.NavigationDrawerItem;

/**
 * Created by christianeyee on 27/02/2016.
 */
public class NavItemListener implements NavigationDrawerAdapter.OnItemClickListener {

    private Context context;

    public NavItemListener(Context context) {
        this.context = context;
    }

    @Override
    public void onItemClick(NavigationDrawerItem item) {
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.drawerLayout.closeDrawers();
        FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager().beginTransaction();

        Log.e(item.getTitle(), "CLICKED");
        Fragment selectedFragment = new MapFragment();
        switch (item.getPosition()) {
            case 0:
                selectedFragment = new MapFragment();
                break;
            case 1:
                selectedFragment = new EarningsFragment();
                break;
            case 2:
                selectedFragment = new AdsFragment();
                break;
            case 3:
                selectedFragment = new AdsFragment();
                break;
            case 4:
                selectedFragment = new AboutFragment();
                break;
            default:
                ((MainActivity) context).finish();
                break;
        }
        fragmentTransaction.replace(R.id.content_frame, selectedFragment);
        fragmentTransaction.commit();
    }

}
