package com.incodeables.kalyesurvey.model;

import java.util.ArrayList;

/**
 * Created by christianeyee on 28/02/2016.
 */
public class Question {
    private Boolean yes = null;

    private String text;
    private String[] choices;

    public Question(String text, String[] choices) {
        this.choices = choices;
        this.text = text;
    }

    public Boolean isYes() {
        return yes;
    }

    public void setYes(boolean yes) {
        this.yes = yes;
    }

    public int getType() {
        /*
        * 0 - yes or no
        * 1 - multiple choice
        * */
        if (choices.length > 2) {
            return 1;
        }
        return 0;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String[] getChoices() {
        return choices;
    }

    public void setChoices(String[] choices) {
        this.choices = choices;
    }

    public static ArrayList<Question> getData() {
        ArrayList<Question> dataList = new ArrayList<>();

        String[] titles = getTexts();
        String[] choices = {"YES", "NO"};

        for (int i=0; i<titles.length; i++) {
            Question q = new Question(titles[i], choices);
            dataList.add(q);
        }
        return dataList;
    }

    private static String[] getTexts() {
        return new String[] {
                "Did you attend the hackathon?",
                "Is the food delicious?",
                "Is programming fun?",
                "Are you confident?",
                "Is it tiring?",
                "Did you sleep?",
        };
    }
}
