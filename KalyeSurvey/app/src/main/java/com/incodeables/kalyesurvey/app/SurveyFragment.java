package com.incodeables.kalyesurvey.app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.incodeables.kalyesurvey.MainActivity;
import com.incodeables.kalyesurvey.R;
import com.incodeables.kalyesurvey.adapter.QuestionAdapter;
import com.incodeables.kalyesurvey.model.Question;

import java.util.Locale;


public class SurveyFragment extends Fragment {
    private TextToSpeech tts;
    private QuestionAdapter adapter;

    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_survey, container, false);

        setUpRecyclerView(view);
        setUpTts();

        return view;
    }

    private void setUpTts() {
        handler = new Handler(Looper.getMainLooper());
        tts = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    Log.e("STATUS", "SUCCESS");
                    tts.setLanguage(Locale.UK);

                    readNext();

                    tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onDone(String utteranceId) {
                            handler.post(new Runnable() {
                                public void run() {
                                    ((MainActivity) getActivity()).listenToAnswer();
                                }
                            });

//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    removeDone();
//                                }
//                            }, 2000);
                        }

                        @Override
                        public void onError(String utteranceId) {
                        }

                        @Override
                        public void onStart(String utteranceId) {
                        }
                    });
                }
            }
        });
    }

    public void readNext() {
        if (adapter.getItemCount() > 0) {
            String text = adapter.getItemText(0);
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, getActivity().getString(R.string.text));
        }
    }

    public void removeDone() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.removeItem(0);
            }
        });
    }

    public void setYes(int position) {
        adapter.setYes(position);
    }

    public void setNo(int position) {
        adapter.setNo(position);
    }

    private void setUpRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        adapter = new QuestionAdapter(getActivity(), Question.getData());
        recyclerView.setAdapter(adapter);

        LinearLayoutManager mLinearLayoutManagerVertical = new LinearLayoutManager(getActivity());
        mLinearLayoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLinearLayoutManagerVertical);

        // default
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }
}
