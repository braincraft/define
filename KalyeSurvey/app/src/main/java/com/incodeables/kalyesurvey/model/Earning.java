package com.incodeables.kalyesurvey.model;

import java.util.ArrayList;

/**
 * Created by christianeyee on 28/02/2016.
 */
public class Earning {
    private String date;
    private String sponsor;
    private String reward;

    public Earning(String date, String sponsor, String reward) {
        this.date = date;
        this.sponsor = sponsor;
        this.reward = reward;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public static ArrayList<Earning> getData() {
        ArrayList<Earning> dataList = new ArrayList<>();

        String[] sponsors = getSponsors();
        String[] dates = getDates();
        String[] rewards = getRewards();

        for (int i=0; i<dates.length; i++) {
            Earning e = new Earning(dates[i], sponsors[i], rewards[i]);
            dataList.add(e);
        }
        return dataList;
    }

    private static String[] getSponsors() {
        return new String[] {};
    }

    private static String[] getDates() {
        return new String[] {};
    }

    private static String[] getRewards() {
        return new String[] {};
    }
}
