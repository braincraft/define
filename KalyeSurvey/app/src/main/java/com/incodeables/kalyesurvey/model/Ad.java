package com.incodeables.kalyesurvey.model;

import java.util.ArrayList;

/**
 * Created by christianeyee on 28/02/2016.
 */
public class Ad {
    private String title;
    private String date;
    private String sponsor;
    private String reward;

    public Ad(String title, String date, String sponsor, String reward) {
        this.title = title;
        this.date = date;
        this.sponsor = sponsor;
        this.reward = reward;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public static ArrayList<Ad> getData() {
        ArrayList<Ad> dataList = new ArrayList<>();

        String[] titles = getTitles();
        String[] sponsors = getSponsors();
        String[] dates = getDates();
        String[] rewards = getRewards();

        for (int i=0; i<dates.length; i++) {
            Ad a = new Ad(titles[i], dates[i], sponsors[i], rewards[i]);
            dataList.add(a);
        }
        return dataList;
    }

    private static String[] getTitles() {
        return new String[] {};
    }

    private static String[] getSponsors() {
        return new String[] {};
    }

    private static String[] getDates() {
        return new String[] {};
    }

    private static String[] getRewards() {
        return new String[] {};
    }
}
