package com.incodeables.kalyesurvey.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.incodeables.kalyesurvey.R;
import com.incodeables.kalyesurvey.adapter.NavigationDrawerAdapter;
import com.incodeables.kalyesurvey.listener.NavItemListener;
import com.incodeables.kalyesurvey.model.NavigationDrawerItem;

/**
 * Created by christianeyee on 24/02/2016.
 */
public class NavigationDrawerFragment extends android.support.v4.app.Fragment {

    private DrawerLayout drawerLayout;
    private RecyclerView recyclerView;
    private NavigationDrawerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        setUpRecyclerView(view);
        getFragmentManager().beginTransaction().replace(R.id.content_frame, new MapFragment()).commit();
        return view;
    }

    private void setUpRecyclerView(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.drawerList);

    }

    public void setUpDrawer(Context context, int fragment, DrawerLayout drawerLayout, final ActionBarDrawerToggle mDrawerToggle, Toolbar toolbar) {
        this.drawerLayout = drawerLayout;
        this.drawerLayout.setDrawerListener(mDrawerToggle);

        // to synchronize the rotation of navigation icon
        this.drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        adapter = new NavigationDrawerAdapter(getActivity(), NavigationDrawerItem.getData());
        adapter.setNavItemListener(new NavItemListener(context));
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }
}
