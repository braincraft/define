# KalyeSurvey

KalyeSurvey is an app that boosts productivity and earnings on the most stressful moments: that is, during Traffic. You'll learn more in our pitch! Watch out.

This Repository contains the following directory
---
1. KalyeSurvey - directory for the mobile app
2. KalyeSurveySite - the website to feature the mobile app
3. Mockups - designs and few assets of KalyeSurvey

Developed by TEAM IN-CODE-ABLES | 2016
---
1. Gian Carlo Sagun
2. John Louise Tan
3. Eduardo Valdez
4. Christopher Vizcarra
5. Joy Angelyn Yee
